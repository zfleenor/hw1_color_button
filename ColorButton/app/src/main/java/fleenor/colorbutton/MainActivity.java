package fleenor.colorbutton;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {
    RelativeLayout rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rl = (RelativeLayout)findViewById(R.id.activity_main);

        final Button Blue_Button = (Button) findViewById(R.id.Blue_Button);
        Blue_Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                rl.setBackgroundColor(Color.BLUE);
            }
        });

        final Button Red_Button = (Button) findViewById(R.id.Red_Button);
        Red_Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                rl.setBackgroundColor(Color.RED);
            }
        });

        final Button Green_Button = (Button) findViewById(R.id.Green_Button);
        Green_Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                rl.setBackgroundColor(Color.GREEN);
            }
        });

        final Button Yellow_Button = (Button) findViewById(R.id.Yellow_Button);
        Yellow_Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                rl.setBackgroundColor(Color.YELLOW);
            }
        });


    }
}
